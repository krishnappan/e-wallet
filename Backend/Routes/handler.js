const UserService=require('../Services/user-service');
const BankService=require('../Services/bank-service.js');
const FriendService=require('../Services/friend-service.js');
const SessionService=require('../Services/session-service.js')
class RouteHandler{
    constructor(){
    }
    signup(req,reply){
        const me=this;
        let userService=new UserService(req);
        userService.signup(req.payload).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        });
    }

        login(req,reply){
        const me=this;
        let userService=new UserService(req);
        userService.login(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }
         referral(req,reply){
        const me=this;
        let userService=new UserService(req);
        userService.referral(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

    
    getUserById(req,reply){
        const me=this
        let userService=new UserService(req);
        userService.getUserById(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }
    updateUserById(req,reply){
        const me=this
        let userService=new UserService(req);
        userService.updateUserById(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

    tHistory(req,reply){
        const me=this;
        let userService=new UserService(req);
        userService.tHistory(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

       recharge(req,reply){
        const me=this;
        let bankService=new BankService(req);
        bankService.recharge(req).then((result)=>{
            //console.log(result);
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }
    getBank(req,reply){
        const me=this;
        let bankService=new BankService(req);
        bankService.getBank(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

    addBank(req,reply){
        const me=this;
        let bankService=new BankService(req);
        bankService.addBank(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

    updateBank(req,reply){
        const me=this;
        let bankService=new BankService(req);
        bankService.updateBank(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }
    removeBank(req,reply){
        const me=this;
        let bankService=new BankService(req);
        bankService.removeBank(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

 
    

    getFriend(req,reply){
        const me=this;
        let friendService=new FriendService(req);
        friendService.getFriend(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

    addFriend(req,reply){
        const me=this;
        let friendService=new FriendService(req);
        friendService.addFriend(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

    sendFriend(req,reply){
        const me=this;
        let friendService=new FriendService(req);
        friendService.sendFriend(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }
    removeFriend(req,reply){
        const me=this;
        let friendService=new FriendService(req);
        friendService.removeFriend(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

      Notifications(req,reply){
        const me=this;
        let sessionService=new SessionService(req);
        sessionService.Notifications(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }
    ShowNotifications(req,reply){
        const me=this;
        let sessionService=new SessionService(req);
        sessionService.ShowNotifications(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }
    PutNotifications(req,reply){
        const me=this;
        let sessionService=new SessionService(req);
        sessionService.PutNotifications(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }

    AskNotification(req,reply){
        const me=this;
        let friendService=new FriendService(req);
        friendService.AskNotification(req).then((result)=>{
           reply(result).code(200);
        },(err)=>{
            reply(err).code(400);
        }); 
    }


}
module.exports=RouteHandler;