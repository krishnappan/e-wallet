const Schema = require('../Schema/dbSchema');
const joi = require('../Schema/joiSchema');
const hash = require('password-hash');
const Joi = require('joi')
const cookie=require('hapi-auth-cookie')
const RouteHandler = require('./handler.js');
class Routes {
    constructor() {
        this.routeHandler = new RouteHandler();
    }
    registerRoutes(server) {
        const me = this;
        server.log("Registering all routes!!");

        server.route({
            method: 'GET',
            path: '/',
            config: {
                auth:false,
                tags: ['api'],
                description: "Home Page",
                handler: (request, reply) => { reply("Home page") }
            }
        })

        server.route({
            method: 'POST',
            path: '/signup',
             
            handler: (req, reply) => me.routeHandler.signup(req, reply),
            config:
            {
                tags: ['api'],
                auth:false,
                description: 'Signup New User',
                validate: {
                    payload: joi.userSchema
                }
            }

        });


        server.route({
            method: 'POST',
            path: '/login',
            
            handler: (req, reply) => me.routeHandler.login(req, reply),
            config:

            {
                 auth:false,
                tags: ['api'],
                description: 'Login user',
                validate: {
                    payload: joi.loginSchema
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/user/{userId}/referral',
           
            handler: (req, reply) => me.routeHandler.referral(req, reply),
            config:

            {
                tags: ['api'],
                auth:'session',
                description: 'Referral Status',
                validate: {
                    params: joi.idSchema
                }
            }
        });
        server.route({
            method: 'GET',
            path: '/user/{userId}/account',
            handler: (req, reply) => me.routeHandler.getUserById(req, reply),
            config:

            {
                 auth:'session',
                tags: ['api'],
                description: 'User Details',
                validate: {
                    params: joi.idSchema
                }
            }
        });
        server.route({
            method: 'PUT',
            path: '/user/{userId}/account',
            handler: (req, reply) => me.routeHandler.updateUserById(req, reply),
            config:

            {
                tags: ['api'],
              auth:'session',
                description: 'Update User Details',
                validate: {
                    payload: joi.userputSchema
                }
            }
        });
        server.route({
            method: 'GET',
            path: '/user/{userId}/wallet',
            handler: (req, reply) => me.routeHandler.tHistory(req, reply),
            config:
            {
                tags: ['api'],
                 auth:'session',
                description: 'Wallet Transaction Details',
                validate: {
                    params: joi.idSchema
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/user/{userId}/notifications',
            handler: (req, reply) => me.routeHandler.Notifications(req, reply),
            config:
            {
                tags: ['api'],
                 auth:'session',
                description: ' Unread Notification',
                validate: {
                    params: joi.idSchema
                }
            }
        });
         server.route({
            method: 'GET',
            path: '/user/{userId}/notification',
            handler: (req, reply) => me.routeHandler.ShowNotifications(req, reply),
            config:
            {
                tags: ['api'],
                 auth:'session',
                description: 'List Notifications',
                validate: {
                    params: joi.idSchema
                }
            }
        });
        server.route({
            method: 'PUT',
            path: '/user/{userId}/notification',
            handler: (req, reply) => me.routeHandler.PutNotifications(req, reply),
            config:
            {
                tags: ['api'],
                 auth:'session',
                description: 'Change status of  Notifications',
                validate: {
                    params: joi.idSchema
                }
            }
        });

        server.route({
            method: 'POST',
            path: '/user/{userId}/notification',
            handler: (req, reply) => me.routeHandler.AskNotification(req, reply),
            config:
            {
                tags: ['api'],
                 auth:'session',
                description: ' Add Borrow Notifications',
                validate: {
                    params: joi.idSchema,
                    payload:joi.borrowSchema
                }
            }
        });


        server.route({
            method: 'PUT',
            path: '/user/{userId}/recharge/{choice}',
            handler: (req, reply) => me.routeHandler.recharge(req, reply),
            config:

            {
                tags: ['api'],
                auth:'session',
                description: 'Money Transfer Methods',
                validate: {
                    params: joi.choiceSchema,
                    payload: joi.transactionSchema
                }
            }
        });

        server.route({
            method: 'POST',
            path: '/user/{userId}/bank',
            handler: (req, reply) => me.routeHandler.addBank(req, reply),
            config:
            {
                tags: ['api'],
               auth:'session',
                description: 'Add Bank Details',
                validate: {
                    payload: joi.bankSchema
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/user/{userId}/bank/{bankName}',
            handler: (req, reply) => me.routeHandler.getBank(req, reply),
            config:
            {
                tags: ['api'],
                auth:'session',
                description: 'Bank Details by bankName',
                validate: {
                    params: joi.bankSchema
                }
            }
        });
        server.route({
            method: 'GET',
            path: '/user/{userId}/bank',
            handler: (req, reply) => me.routeHandler.getBank(req, reply),
            config:
            {
                tags: ['api'], 
                auth:'session',
                description: 'Bank Details',
                validate: {
                    params: joi.bankSchema
                }
            }
        });

        server.route({
            method: 'PUT',
            path: '/user/{userId}/bank/{bankName}',
            handler: (req, reply) => me.routeHandler.updateBank(req, reply),
            config:

            {
                tags: ['api'],  
               auth:'session',
                description: 'Update Bank Details',
                validate: {
                    payload: joi.bankSchema,
                    params: joi.idSchema
                }
            }
        });

        server.route({
            method: 'DELETE',
            path: '/user/{userId}/bank/{bankName}',
            handler: (req, reply) => me.routeHandler.removeBank(req, reply),
            config:

            {
                tags: ['api'],  
                auth:'session',
                description: 'Remove Bank Details',
                validate: {
                    params: joi.idSchema
                }
            }
        });


        server.route({
            method: 'PUT',
            path: '/user/{userId}/friend',
            handler: (req, reply) => me.routeHandler.addFriend(req, reply),
            config:
            {
                tags: ['api'],  
                auth:'session',
                description: 'Add Friend Details',
                validate: {
                    payload: joi.idSchema
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/user/{userId}/friend',
            handler: (req, reply) => me.routeHandler.getFriend(req, reply),
            config:
            {
                tags: ['api'],  
                auth:'session',
                description: 'Friend  Details',
                validate: {
                    params: joi.idSchema
                }
            }
        });
        server.route({
            method: 'GET',
            path: '/user/{userId}/friend/{mobileNumber}',
            handler: (req, reply) => me.routeHandler.getFriend(req, reply),
            config:
            {
                tags: ['api'],  
               auth:'session',
                description: 'Friend  Details  by mobile Number',
                validate: {
                    params: joi.idSchema
                }
            }
        });

        server.route({
            method: 'PUT',
            path: '/user/{userId}/friend/{mobileNumber}/send',
            handler: (req, reply) => me.routeHandler.sendFriend(req, reply),
            config:

            {
                tags: ['api'], 
              auth:'session',
                description: 'Send Money to Friend ',
                validate: {
                    payload: joi.transactionSchema,
                    params: joi.idSchema
                }
            }
        });

        server.route({
            method: 'DELETE',
            path: '/user/{userId}/friend/{mobileNumber}',
            handler: (req, reply) => me.routeHandler.removeFriend(req, reply),
            config:

            {
                tags: ['api'],  
                auth:'session',
                description: 'Remove Friend ',
                validate: {
                    params: joi.idSchema
                }
            }
        });

        server.route({
            method: 'PUT',
            path: '/user/{userId}/send/{mobileNumber}',
            handler: (req, reply) => me.routeHandler.sendFriend(req, reply),
            config:

            {
                tags: ['api'],  
               auth:'session',
                description: 'Send Money to MobileNumber',
                validate: {
                    payload: joi.transactionSchema,
                    params: joi.idSchema
                }
            }
        });


        server.route({
            method: 'GET',
            path: '/logout',
            config: {
                tags:['api'],
                auth: false,
                handler: (req, reply)=> {
                    req.cookieAuth.clear();
                    return reply('success');
                }
            }
        });


    }

}
module.exports = Routes