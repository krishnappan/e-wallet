const userService = require('../Services/user-service.js');
const bankService = require('../Services/bank-service.js');
const friendService = require('../Services/friend-service.js');
const sessionService = require('../Services/session-service.js');
const assert = require('assert')
const uuid = require('uuid')
const dbSchema=require('../Schema/dbSchema')

const dbUser=dbSchema.User;
const dbWallet=dbSchema.Wallet;
const dbTransaction=dbSchema.Transaction;
const dbFriend=dbSchema.Friend;
const loginreq={
    "payload": {
    "mobileNumber": 9842915287,
    "password": "samsung16",
    },"cookieAuth":
    {
        "set":function(session,value){
            }
    }
}
//invalid mobile Number
const loginreq1={
    "payload": {
    "mobileNumber": 9842915288,
    "password": "samsung16",
    },"cookieAuth":
    {
        "set":function(session,value){
            }
    }
}
//invalid Password
const loginreq2={
    "payload": {
    "mobileNumber": 9842915287,
    "password": "samsung161",
    },"cookieAuth":
    {
        "set":function(session,value){
            }
    }
}
//correct (start user) Details
const signup={
    "mobileNumber":9008765111,
    "emailId":"harish91@gmail.com",
    "password":"harish",
    "firstName":"Harish",
    "lastName":"Kumar",
    "referralcode":"13142730-5873-11e6-934a-d7f183ab3211"
}
//incorrect Details
const signup1={
    "mobileNumber":"",
    "emailId":"harish91@gmail.com",
    "password":"",
    "firstName":"Harish",
    "lastName":"Kumar",
    "referralcode":"13142730-5873-11e6-934a-d7f183ab3211"
}
//signup normal user
const signup2={
    "mobileNumber":9008765112,
    "emailId":"Ramesh@gmail.com",
    "password":"ramesh",
    "firstName":"Ramesh",
    "lastName":"Kumar",
    "referralcode":"ce93b9d0-5873-11e6-934a-d7f183ab3211"
}
//invalid referral code
const signup3={
     "mobileNumber":9008765113,
    "emailId":"Suresh@gmail.com",
    "password":"Suresh",
    "firstName":"Suresh",
    "lastName":"Kumar",
    "referralcode":"f2b901e0-5160-11e6-ae0c-6f2c6aa81049"
}
//Get User By Id
const userId={
    "params":{
        "userId":"13142730-5873-11e6-934a-d7f183ab3211"
    },
    "auth":{
        "credentials":{
            "userId":"13142730-5873-11e6-934a-d7f183ab3211"
        }
    }
}

//invalid get user by id
const userId1={
    "params":{
        "userId":"13142730-5873-11e6-934a-d7f183ab3221"
    },
    "auth":{
        "credentials":{
            "userId":"13142730-5873-11e6-934a-d7f183ab3211"
        }
    }
}
//get referral of normal user //also transaction history
const userId2={
    "params":{
        "userId":"ce93b9d0-5873-11e6-934a-d7f183ab3211"
    },
    "auth":{
        "credentials":{
            "userId":"ce93b9d0-5873-11e6-934a-d7f183ab3211"
        }
    }
}
// Output For Get User By ID
const getaccount={
  "count": 4,
  "emailId": "krishna95@gmail.com",
  "firstName": "Krishna",
  "id": "73aa4a2a-db64-46f7-892d-e3485d844242",
  "lastName": "Subramanian",
  "mobileNumber": 9842915287,
  "password": "sha1$3cc7e402$1$a4c0c92764ca3a07826c792b6a61ee8ffeade4fe",
  "referralcode": "",
  "userId": "13142730-5873-11e6-934a-d7f183ab3211"
}
//output for get transaction
const History=[
  {
    "balance": 0,
    "id": "5f59009a-08fc-4694-9541-0e542d283560",
    "star": false,
    "userId": "13142730-5873-11e6-934a-d7f183ab3211"
  },
  []
]

const updateId={
 "params":{
        "userId":"13142730-5873-11e6-934a-d7f183ab3211"
    },
    "auth":{
        "credentials":{
            "userId":"13142730-5873-11e6-934a-d7f183ab3211"
        }
    },
    "payload":{
        "firstName":"Krishna",
        "lastName":"Subramanian",
        "emailId":"krishna95@gmail.com"
    }
} 
const updateId1={
     "params":{
        "userId":"13142730-5873-11e6-934a-d7f183ab3212"
    },
    "auth":{
        "credentials":{
            "userId":"13142730-5873-11e6-934a-d7f183ab3211"
        }
    },
    "payload":{

    }
}
describe('Testing Login User Services\n',function(){
    it('Login Success',function(done){
        const  userservice=new userService();
        userservice.login(loginreq).then(function(r){
            //console.log(r);
            assert.equal(r,"13142730-5873-11e6-934a-d7f183ab3211");
            //console.log("hello "+r);
            done();
        },
        function(error) { 
            res="error";
            console.log("error")
            done();
        });
    }) 
    it('Login Invalid',function(done){
        const  userservice=new userService();
        userservice.login(loginreq1).then(function(r){
            assert.equal(r,"invalid");
            done();
        },
        function(error) { 
            res="error";
            console.log("error")
            done();
        });
    }) 
    it('Login Invalid Credentials',function(done){
        const  userservice=new userService();
        userservice.login(loginreq2).then(function(r){
            assert.equal(r,"invalid");
            done();
        },
        function(error) { 
            res="error";
            console.log("error")
            done();
        });
    }) 
})
describe('Testing GetUser info Services\n',function(){
    it('GET Userinfo Success',function(done){
        const  userservice=new userService();
        userservice.getUserById(userId).then(function(r){
        //console.log(r);
        assert.deepEqual(r,getaccount);
        done();
        },
        function(error){
            res="error";
            console.log("error")
            done();
        });
    })
    it('Get Invalid Userinfo',function(done){
        const  userservice=new userService();
        userservice.getUserById(userId1).then(function(r){
        //console.log(r);
        assert.equal(r,"System Violation"); 
        done();
        },
        function(error){
            res="error";
            console.log("error")
            done();
        });
    })
    })

describe('Testing History   of transactions for given users',function () {
    it('History',function(done){
        const  userservice=new userService();
        userservice.tHistory(userId).then(function(r){
        assert.deepEqual(r,History); 
        done();
        },
        function(error){
            res="error";
            console.log("error")
            done();
        });
    })
    it(' Invalid History info',function(done){
        const  userservice=new userService();
        userservice.tHistory(userId1).then(function(r){
        console.log(r);
        assert.equal(r,"System Violation"); 
        done();
        },
        function(error){
            res="error";
            console.log("error")
            done();
        });
    })
})



describe('Testing UpdateUser info Services\n',function(){
    it('Update Userinfo Success',function (done) {
        this.timeout(20000)
        const userservice=new userService();
        userservice.updateUserById(updateId).then(function (res) {
            assert.equal(res,"success")
            done();
        },function (error) {
            res="error";
            console.log("error")
            done();
        })
    })
    it('Update Invalid Userinfo',function(done){
        const  userservice=new userService();
        userservice.updateUserById(updateId1).then(function(r){
       // console.log(r);
        assert.equal(r,"System Violation"); 
        done();
        },
        function(error){
            res="error";
            console.log("error")
            done();
        });
    })
})


describe('Testing Signup User Services\n',function(){
    it('Signup Success using star User',function(done){
        const  userservice=new userService();
        userservice.signup(signup).then(function(r){
            assert.equal(r,"success");
            done();
        },
        function(error) { 
            res="error";
            console.log("error")
            done();
        });
    }) 

     it('Signup ERROR',function(done){
        const  userservice=new userService();
        userservice.signup(signup1).then(function(r){
            assert.equal(r,"error");
            done();
        },
        function(error) { 
            res="error";
            console.log("error")
            done();
        });
    })
    it('Signup using invalid referralcode',function(done){
        const  userservice=new userService();
        userservice.signup(signup3).then(function(r){
            assert.equal(r,"invalid");
            done();
        },
        function(error) { 
            res="error";
            console.log("error")
            done();
        });
    })
    it('Signup using Normal User',function(done){
        const  userservice=new userService();
        userservice.signup(signup2).then(function(r){
            assert.equal(r,"success");
            done();
        },
        function(error) { 
            res="error";
            console.log("error")
            done();
        });
    })
})
 



describe('Testing Referral Status of all Type users',function () {
    it('Star User',function(done){
        const  userservice=new userService();
        userservice.referral(userId).then(function(r){
        console.log(r);
        assert.equal(r,8); 
        done();
        },
        function(error){
            res="error";
            console.log("error")
            done();
        });
    })
     it('Normal User',function(done){
        const  userservice=new userService();
        userservice.referral(userId2).then(function(r){
        console.log(r);
        assert.equal(r,4); 
        done();
        },
        function(error){
            res="error";
            console.log("error")
            done();
        });
    })
    it('Status Invalid Userinfo',function(done){
        const  userservice=new userService();
        userservice.referral(updateId1).then(function(r){
        console.log(r);
        assert.equal(r,"System Violation"); 
        done();
        },
        function(error){
            res="error";
            console.log("error")
            done();
        });
    })
})

