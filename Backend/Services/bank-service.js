co = require('co');
const thinky = require('thinky')()
const hash = require('password-hash')
const dbSchema = require('../Schema/dbSchema')
const dateFormat=require('dateformat')
const cookie = require('hapi-auth-cookie')
var r = thinky.r;
class BankService {
    constructor() {
        this.Bank = dbSchema.Bank
        this.Wallet = dbSchema.Wallet
        this.Transaction = dbSchema.Transaction
        this.Notification=dbSchema.Notification
    }
    addBank(req) {
        const me = this;
        let payload = req.payload;
        var bank = {
            userId: req.params.userId,
            bankName: payload.bankName,
            accNo: hash.generate(payload.accNo),
            cardNo: hash.generate(payload.cardNo)
        };
        return co(function* () {
            try {
                if(req.auth.credentials.userId!=req.params.userId)
                return "System Violation";
                yield me.Bank.save(bank)
                return "success"
            } catch (error) {
            }
        })
    }

    getBank(req) {
        const me = this;
        return co(function* () {
            try {
                if(req.auth.credentials.userId!=req.params.userId)
                return "System Violation";
                if(req.params.bankName)
                {var data = yield me.Bank.filter({ userId: req.params.userId,bankName:req.params.bankName }).run()
                 return data[0];
                }
                else
               { var data= yield me.Bank.filter({ userId: req.params.userId }).run()
                 return data;
               }   
                
            }
            catch (error) {
            }
        })
    }
    updateBank(req) {
        const me = this;
        let payload = req.payload;
        return co(function* () {
            try {
                if(req.auth.credentials.userId!=req.params.userId)
                return "System Violation";
                    yield me.Bank.filter({ bankName: req.params.bankName,userId:req.params.userId }).update({ bankName: payload.bankName,accNo:hash.generate(payload.accNo),cardNo:hash.generate(payload.cardNo) }).run()
                return "success"
            }
            catch (error) {
            }
        })
    }
    removeBank(req) {
        const me = this;
        return co(function* () {
            try {
                if(req.auth.credentials.userId!=req.params.userId)
                return "System Violation";
                yield r.table("bank").filter({userId:req.params.userId,bankName:req.params.bankName}).delete();
                 // console.log("Hai")
                return "success";
                }
            catch (err) {
                console.log(err);
                throw err;
                //throw error;
            }
        })
    }

    recharge(req) {
        const me = this;
        let payload = req.payload;
        let choice=req.params.choice;
        var transaction = {
                    Tdate: dateFormat(),
                    amount: payload.amount,
                    userId: req.params.userId
                };
        var notification={
            time:dateFormat(),
             userId:req.params.userId,
             notificationType:"Balance"
        }
        return co(function* () {
            try {
                if(req.auth.credentials.userId!=req.params.userId)
                return "System Violation";
                if(choice==1)
                {
                    transaction.from="Wallet";
                    transaction.to="Recharge";
                    transaction.notation="-";
                    var bal = yield me.Wallet.filter({ userId: req.params.userId });
                    var n=(bal[0].balance)-(payload.amount);
                    notification.msg="Mobile Recharge(debit): "+payload.amount+" New Balance:"+n;
                    if ((bal[0].balance - payload.amount) >= 0) {
                         yield r.table('wallet').filter({ userId: req.params.userId }).update({balance:(r.expr(bal[0].balance).sub(payload.amount))}).run()
                        yield me.Transaction.save(transaction);
                        yield me.Notification.save(notification);
                        return "success"
                    }
                    else {
                        return "insufficient"
                    }
                }
                else if(choice==2)
                {
                    transaction.from="Wallet";
                    transaction.to="Shop";
                     transaction.notation="-";
                    var bal = yield me.Wallet.filter({ userId: req.params.userId });
                    var n=(bal[0].balance)-(payload.amount)
                    notification.msg="Bill paid to shop(debit) : "+payload.amount+" New Balance:"+n;
                    if ((bal[0].balance - payload.amount) >= 0) {
                         yield r.table('wallet').filter({ userId: req.params.userId }).update({balance:(r.expr(bal[0].balance).sub(payload.amount))}).run()
                        yield me.Transaction.save(transaction);
                        yield me.Notification.save(notification);
                        return "success"
                    }
                    else {
                        return "insufficient"
                    }
                }
                else if(choice==3)
                {
                    if(payload.from!="" && payload.from!=undefined)
                    {
                        transaction.from=payload.from;
                        var data = yield me.Bank.filter({ userId: req.params.userId,bankName:payload.from}).run()
                        if(data[0]==undefined)
                           { return "wrong";}
                        else if(data[0].bankName)
                           { transaction.from=payload.from;}
                        else
                            return "wrong"
                    }
                   else
                        return "wrong"
                    transaction.to="Wallet";
                     transaction.notation="+";
                    var bal = yield me.Wallet.filter({ userId: req.params.userId });
                    var n=(bal[0].balance)+(payload.amount)
                    notification.msg="Wallet Recharge(credit): "+payload.amount+" New Balance:"+n;
                    yield r.table('wallet').filter({ userId: req.params.userId }).update({balance:(r.expr(bal[0].balance).add(payload.amount))}).run()
                    yield me.Transaction.save(transaction);
                    yield me.Notification.save(notification);
                    return "success";
                
                }
                
            }
            catch (error) {
            }
        })


    }





}
module.exports = BankService;