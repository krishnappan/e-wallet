co = require('co');
const thinky = require('thinky')()
const hash = require('password-hash')
const dbSchema = require('../Schema/dbSchema')
const dateFormat = require('dateformat')
const cookie = require('hapi-auth-cookie')
var r = thinky.r;
class FriendService {
    constructor() {
        this.User = dbSchema.User
        this.Friend = dbSchema.Friend
        this.Transaction = dbSchema.Transaction
        this.Wallet = dbSchema.Wallet
        this.Notification = dbSchema.Notification
    }

    checkUtility(user, mobile) {
        const me = this;
        return co(function* () {
            try {
                var id = (yield me.User.filter({ mobileNumber: mobile }).run())[0].userId;
                if (id == user)
                    return 1;
                var data = yield me.Friend.filter({ userId: user }).run()
                var list = data[0].list

                for (let i = 0; i < list.length; i++) {
                    // console.log(list[i],id);
                    if (list[i] == id)
                    { return 0; }
                }
                return 2;

            } catch (error) {
            }
        })
    }


    addFriend(req) {
        const me = this;
        let user = req.params.userId;
        let mobile = req.payload.mobileNumber;
        var notification = {
            time: dateFormat(),
            userId: req.params.userId,
            notificationType: "Friend"
        }
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation";

                var data = yield me.User.filter({ mobileNumber: mobile }).run();
                var details = yield me.User.filter({ userId: user }).run()
                var already = (yield me.checkUtility(user, mobile))

                //mobile number given is already a user
                if (data[0] == undefined)
                { return "wrong"; }
                else if (already == 0)
                { return "already" }
                else if (already == 1)
                { return "invalid" }
                else if (data[0].firstName && already == 2)
                { notification.msg = "New Friend Added!!! Name:" + data[0].firstName + "" + data[0].lastName }
                else
                    return "wrong"

                //adding friends and notifications
                let friendId = data[0].userId;
                yield me.Friend.filter({ userId: user }).update({ list: r.row('list').append(friendId) });
                yield me.Notification.save(notification)

                let not = {
                    userId: friendId,
                    time: dateFormat(),
                    notificationType: "Friend",
                    msg: "New Friend Added!!! Name:" + details[0].firstName + "" + details[0].lastName
                }
                yield me.Friend.filter({ userId: friendId }).update({ list: r.row('list').append(user) });
                yield me.Notification.save(not);
                return "success"
            } catch (error) {
            }
        })
    }

    getFriend(req) {
        const me = this;
        let user = req.params.userId;
        let mobile = req.params.mobileNumber;
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation";
                if (mobile) {
                    var details = yield me.User.filter({ mobileNumber: mobile }).run()
                    return details[0];
                }
                else {
                    var data = yield me.Friend.filter({ userId: user }).run()
                    var list = data[0].list
                    var details = []
                    for (let i = 0; i < list.length; i++) {
                        var temp = yield me.User.filter({ userId: list[i] }).run()
                        // console.log(temp[0]);
                        details.push(temp[0]);

                    }
                    return details;
                }
            } catch (error) {
            }
        })
    }

    sendFriend(req) {
        const me = this;
        let user = req.params.userId;
        let mobile = req.params.mobileNumber;
        let transaction = {
            Tdate: dateFormat(),
            amount: req.payload.amount,
            userId: req.params.userId,
            notation: '-'
        };
        let trans = {
            Tdate: transaction.Tdate,
            amount: req.payload.amount,
            notation: '+'
        };
        var notification = {
            time: dateFormat(),
            userId: req.params.userId,
            notificationType: "Balance"
        }
        var not = {
            time: dateFormat(),
            notificationType: "Balance"
        }
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation";
                if (transaction.amount > 0) {
                    var details = (yield me.User.filter({ mobileNumber: mobile }))
                    var output = []
                    var friendId = details[0].userId;
                    var friendName = details[0].firstName;
                    not.userId = friendId;
                    var userName = (yield me.User.filter({ userId: user }))[0].firstName;
                    transaction.from = "Wallet";
                    transaction.to = friendName;
                    var bal = yield me.Wallet.filter({ userId: user });
                    var bal1 = yield me.Wallet.filter({ userId: friendId });
                    var y = bal1[0].balance + trans.amount;
                    var x = bal[0].balance - trans.amount;
                    notification.msg = "Money Transferred Successfully(Amount=" + trans.amount + ") to " + friendName + "  New Balance:" + x;
                    not.msg = "Money Received (Amount=" + trans.amount + ") from " + userName + " New Balance:" + y;
                    if ((bal[0].balance - req.payload.amount) >= 0) {
                        yield r.table('wallet').filter({ userId: user }).update({ balance: (r.expr(bal[0].balance).sub(req.payload.amount)) }).run()
                        yield me.Transaction.save(transaction);
                        yield me.Notification.save(notification);
                        output.push(transaction)
                        trans.from = userName;
                        trans.to = "Wallet";
                        trans.userId = friendId;
                        yield r.table('wallet').filter({ userId: friendId }).update({ balance: (r.expr(bal1[0].balance).add(req.payload.amount)) }).run()
                        yield me.Transaction.save(trans);
                        yield me.Notification.save(not);
                        output.push(trans)
                        return "success"
                    }
                    else {
                        return "insufficient"
                    }
                }
                else {
                    return "wrong"
                }
            } catch (error) {
            }
        })
    }


    removeFriend(req) {
        const me = this;
        let user = req.params.userId;
        let mobile = req.params.mobileNumber;
        var notification = {
            time: dateFormat(),
            userId: req.params.userId,
            notificationType: "Friend"
        }
        var not = {
            time: dateFormat(),
            notificationType: "Friend"
        }
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation";
                let friend = yield me.User.filter({ mobileNumber: mobile }).run();
                let friendId = friend[0].userId;
                let friendName = friend[0].firstName + " " + friend[0].lastName;

                notification.msg = friendName + " removed you !!! "
                let data1 = yield me.Friend.filter({ userId: user }).run()
                let list1 = data1[0].list
                let idx1 = list1.indexOf(friendId);
                if (idx1 > -1)
                    list1.splice(idx1, 1);
                yield me.Friend.filter({ userId: user }).update({ list: list1 });
                yield me.Notification.save(notification)

                not.userId = friendId;
                let x = yield me.User.filter({ userId: user }).run();
                not.msg = x[0].firstName + " " + x[0].lastName + " removed you !!!";
                let data2 = yield me.Friend.filter({ userId: friendId }).run()
                let list2 = data2[0].list
                let idx2 = list2.indexOf(user);
                if (idx2 > -1)
                    list2.splice(idx2, 1);
                yield me.Friend.filter({ userId: friendId }).update({ list: list2 });
                yield me.Notification.save(not);
                return "success"
            } catch (error) {
            }
        })
    }
    AskNotification(req) {

        const me = this;
        let user = req.params.userId;
        let mobile = req.payload.mobileNumber;
        let amount = req.payload.amount;
        var notification = {
            time: dateFormat(),
            notificationType: "ASK"
        }
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation";
                var friendId = (yield me.User.filter({ mobileNumber: mobile }).run())[0].userId;
                var details = yield me.User.filter({ userId: user }).run()
                notification.msg = "Your Friend Name:" + details[0].firstName + "" + details[0].lastName + "  asked for Rs." + amount + "(Borrow Request)"
                notification.userId=friendId;
                yield me.Notification.save(notification);
                return "success";
            } catch (error) {
                throw error
            }

        })

    }

}
module.exports = FriendService;