const co = require('co');
const thinky = require('thinky')()
const hash = require('password-hash')
const uuid = require('uuid')
const dbSchema = require('../Schema/dbSchema')
const boom = require('boom')
const cookie = require('hapi-auth-cookie')
const dateFormat = require('dateformat')
var r = thinky.r;
class SessionService {
    constructor() {
        this.User = dbSchema.User
        this.Wallet = dbSchema.Wallet
        this.Transaction = dbSchema.Transaction
        this.Friend = dbSchema.Friend
        this.Notification = dbSchema.Notification
    }
    Notifications(req) {
        const me = this;
        let user = req.params.userId;
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation"; 
                 var data = yield me.Notification.filter({ userId: user }).run()
                 var res=0;
                for(var i=0;i<data.length;i++)
                 {
                     if(data[i].read==false)
                        res+=1;
                 }
                 return res;
            }
            catch (error) {
                return error
            }
        })
    }
    ShowNotifications(req) {
        const me = this;
        let user = req.params.userId;
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation"; 
                 var data = yield me.Notification.filter({ userId: user }).run()
                 return data;
                 
            }
            catch (error) {
                return error
            }
        })
    }

     PutNotifications(req) {
        const me = this;
        let user = req.params.userId;
        let time=req.payload.time;
        return co(function* () {
            try {
                if (req.auth.credentials.userId != req.params.userId)
                    return "System Violation"; 
                     yield me.Notification.filter({ userId: user ,time:time}).update({read:true}).run()

                 return "success";
            }
            catch (error) {
                return error
            }
        })
    }

}
module.exports = SessionService;